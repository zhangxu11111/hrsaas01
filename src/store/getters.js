const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,

  // 共享用户姓名
  username: state => state.user.UserInfo.username,
  // 共享用户id
  userId: state => state.user.UserInfo.userId,
  // 共享用户头像
  staffPhoto: state => state.user.UserInfo.staffPhoto
}
export default getters
