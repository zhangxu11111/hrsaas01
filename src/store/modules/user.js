import { getToken, setToken, removeToken, setTimeStamp } from '@/utils/auth.js'
import { login, getUserProfile, getUserDetailById } from '@/api/user.js'

// 储存值
const state = {
  // 共享token状态
  token: getToken(), // 获取缓存的token
  // 共享UserInfo信息
  UserInfo: {}
}
// 修改值
const mutations = {
  // 修改token
  setToken(state, payload) {
    // 更新token数据
    state.token = payload
    // 在修改缓存的token
    setToken(payload)
  },
  // 退出 销毁token
  removeToken(state) {
    state.token = null // 清空共享token值
    removeToken()// 删除本地缓存token
  },
  // 修改用户信息
  setUserInfo(state, payload) {
    state.UserInfo = payload
  },
  // 删除用户信息
  removeUserInfo(state) {
    state.UserInfo = {}
  }
}
// 异步处理
const actions = {
  // 封装一个登录 token 获取
  async login(context, data) {
    const res = await login(data)
    // 赋值token
    context.commit('setToken', res)
    // 调用时间戳
    setTimeStamp()
  },
  // 封装一个用户信息 获取
  async getUserInfo({ commit }) {
    // 调用封装用户axios
    const result = await getUserProfile()
    // 调用获取图片 axios
    const res = await getUserDetailById(result.userId)
    // 修改UserInfo信息   把获取到的两个值存放在一起
    commit('setUserInfo', { ...result, ...res })
    // 一个伏笔几日后使用
    return result
  },
  // 退出功能
  logout({ commit }) {
    commit('removeToken')
    commit('removeUserInfo')
  }

}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
