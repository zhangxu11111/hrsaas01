/** *
 *
 * 获取组织架构数据
 * **/
import request from '@/utils/request'
export function getDepartments() {
  return request({
    url: '/company/department'
  })
}
// 封装删除接口
export function delDepartments(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
  })
}
// 封装新增接口
export function addDepartments(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}
