// 注册自定义指令
// 处理图片异常的基本手段  检测图片dom的error事件
export const imagerror = {
  // 指定的参数
  // 当指令作用在dom对象插入元素之后触发
  // 讲指令作用在图片上
  insered(dom, options) {
    // options是一个对象 值在value上
    // 图片只要异常就会处理默认图片
    dom.onerror = function() {
      dom.res = options.value // value 从哪里来需要传值过来
    }
  }
}
