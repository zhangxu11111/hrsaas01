import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'
const timeKey = 'vue_admin_template_Time'
export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
// 获取时间戳
export function getTimeStamp() {
  return Cookies.get(timeKey)
}
// 存入时间戳
export function setTimeStamp() {
  // 此处可以不用设置返回值因不需要读取
  Cookies.set(timeKey, Date.now()) // 存入当前时间
}
