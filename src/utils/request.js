// 导入axios
import axios from 'axios'
// 导入message信息
import { Message } from 'element-ui'
// 导入 store 拿到请求头
import store from '@/store'
import router from '@/router'
// 导入之前存储好的时间戳
import { getTimeStamp } from '@/utils/auth'
// 设置一个固定时间戳
const TimeOut = 5400 // 此处为毫秒下面需要除法处理
// 创建axios新实例对象
const services = axios.create({
  // axios的配置
  baseURL: process.env.VUE_APP_BASE_API, // 取node 的环境变量
  timeout: 5000 // 当超过当前毫米 就算超时
})
// 请求拦截器
services.interceptors.request.use(config => {
  // 设置一个请求头 的拦截器
  if (store.getters.token) {
    // 检擦token是否过期
    if (CheckTimeOut()) {
      // 如果成立 为true 就认为超时 就调用删除 touken 和退出
      store.dispatch('user/logout')
      // 跳转路由
      router.push('/login')
      // 成立已经超时就不需要执行下面代码就return promise对象
      return Promise.reject(new Error('您的token已超时从新登录'))
    }
    // 如果请求头有的话 就设置请求头
    config.headers.Authorization = `Bearer ${store.getters.token}`
  }
  // 判断是否存在有请求头
  // 一定要把confing 返回出去
  return config
}, error => {
  // 因为此信息出现只要自己输入错误情况下弹出所以不要提示
  // 直接return promise 对象
  return Promise.reject(new Error(error))
})
// 相应拦截器
services.interceptors.response.use(response => {
  // 先结构赋值出response
  const { success, message, data } = response.data
  // 判断success是否为true
  if (success) {
    // 如果值为true就抛出值
    return data
  } else {
    // 值为true就抛出错误消息
    Message.error(message)
    // 在返回出reject
    return Promise.reject(new Error(message))
  }
}, error => {
  // 判断状态码   此状态码相同就代表已经touken失效
  if (error.response && error.response.data && error.response.data.success === 10002) {
    // 失效后删除token 和 缓存
    store.dispatch('user/logout')
    // 跳转路由
    router.push('/login')
  }
  // 错误了弹出错误消息
  Message.error(error.message)
  // 返回一个错误信息
  return Promise.reject(error)
})

// 检查时间是否过期
function CheckTimeOut() {
  var currentTime = Date.now() // 获取当前时间戳
  var timeStamp = getTimeStamp()// 从缓存中读取时间戳
  return (currentTime - timeStamp) / 1000 > TimeOut
}

// 导出
export default services
