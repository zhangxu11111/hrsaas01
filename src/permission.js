// 导入 store 获取token
// import store from '@/store'
// 导入roter 配置路由守卫
import router from '@/router'
import store from '@/store'
// 导入插件
import nProgress from 'nprogress'
import 'nprogress/nprogress.css'
// 定义白名单
const whiteList = ['/login', '/404']

// 添加前守卫
router.beforeEach(async(to, from, next) => {
  nProgress.start()// 开启进度条
  // 先判断是否存在有token
  if (store.getters.token) {
    // 存在token 判断当前页面是否在 login页面 是的话跳转到首页
    if (to.path === '/login') {
      // 如果在登录页就跳转到首页
      next('/')
      // 关闭进度条
      nProgress.done()
      // 如果没有在登录页
    } else {
      // 获取用户信息 因为需要一登陆进入主要才需要发送请求获取
      // 如何判断是否之前已经进入过登录页面 判断 userid是否有值 ，id没值就获取有值就放行
      // 如何拿到userId 怕嵌套层次过多可以直接开放出来userId
      if (!store.getters.userId) {
      // 没有id就获取   因为此方法是在异步执行的 所以 需要通过 async 和await 转为 必须等待到有值才可以执行下一步
        await store.dispatch('user/getUserInfo')
      }
      // 直接放行
      next()
    }
  } else {
    // 判断 白名单里的值
    if (whiteList.indexOf(to.path) > -1) {
      next()
    } else {
      next('/login')
      // 关闭进度条
      nProgress.done()
    }
  }
})
router.afterEach(() => nProgress.done()) // 关闭进度条`
